from django.urls import path
from .views import *

app_name = 'lab-3'

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add')
]
