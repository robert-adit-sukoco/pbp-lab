from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

# Create your views here


@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


@login_required(login_url="/admin/login/")
def add_friend(request):
    context = {}

    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        if request.method == 'POST':
            return redirect('lab-3:index')

    context['form'] = form
    return render(request, "lab3_form.html", context)
