1. Apakah perbedaan antara JSON dan XML?
   
   JSON adalah singkatan dari <i>JavaScript Object Notation</i>. JSON menggunakan tipe data serupa <i>object</i> yang terdapat pada JavaScript. XML adalah singkatan dari <i>eXtensible Markup Language</i>. Bentuk dari data XML serupa dengan <i>tags</i> yang terdapat pada HTML.


2. Apakah perbedaan antara HTML dan XML?
   
   HTML (<i>HyperText Markup Language</i>) adalah bahasa yang digunakan untuk <i>design</i> dan <i>formatting</i> halaman web. XML (<i>eXtensible Markup Language</i>) adalah bahasa yang digunakan untuk mengirim data berupa <i>response</i>. Meski bentuk dari HTML dan XML serupa, namun keduanya memiliki fungsi yang berbeda.
   
