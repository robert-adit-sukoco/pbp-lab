from django.db import models

# Create your models here.
class Note(models.Model):
    to_who = models.CharField(max_length=20)
    title = models.CharField(max_length=20)
    from_who = models.CharField(max_length=20)
    message = models.CharField(max_length=100)
