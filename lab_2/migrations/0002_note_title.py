# Generated by Django 3.2.7 on 2021-09-26 09:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='title',
            field=models.CharField(default='note_title', max_length=20),
            preserve_default=False,
        ),
    ]
