from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml_page'),
    path('json', json, name='json_page'),
]
