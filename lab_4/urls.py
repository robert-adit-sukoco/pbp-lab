from django.urls import path
from .views import *

app_name = 'lab-4'

urlpatterns = [
    path('', index, name='index'),
    path('note-list', note_list, name='note_list'),
    path('add-note', add_note, name='add_note')
]
